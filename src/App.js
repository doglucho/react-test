import { useState } from "react";
import Title from './Title'

function App() {
  const [showText, setShowText] = useState(false)
  const users = ['pepe', 'pedro', 'santi']
  function toggleText() {
    setShowText(!showText)
  }

  return (
    <div className='father'>
      <Title text = 'ciao'/>
      <Title text = 'byebye'/>
      <button onClick={ toggleText }>Click me</button>
      { showText && <p>hola</p> }
      { users.map((user) => <p key={ user }>{ user }</p> )}
    </div>
  )
}

export default App;
