import { render, screen } from '@testing-library/react';
import App from './App';

test('renders button tag', () => {
  render(<App />);
  const linkElement = screen.getByRole(/button/i);
  expect(linkElement).toBeInTheDocument();
});
